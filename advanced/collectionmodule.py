import collections

student = collections.namedtuple('student', ['name', 'age', 'hobby'])

#student = collections.namedtuple('student', [name, age, marks])

s1 = student("Vikas",30, "Swimming")
print(s1.hobby)

#Deque
q = collections.deque([10, 20, 30, 40, 50])
print(q)

q.appendleft(5)
print(q)