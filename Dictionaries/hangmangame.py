word = "vikas"
letterlist = list(word)
print(letterlist)
letterlistcount = len(letterlist)
totalattempt = 10
lives = 5
correctcounter = 0


for attempt in range(totalattempt):
    userletter = input("Enter letter:")
    if userletter in letterlist and len(userletter) == 1:
        print("Correct")
        correctcounter += 1
        print("Total correct count:", correctcounter)
        if correctcounter == letterlistcount:
            print("You won the game")
            break
        else:
            print("Way to go")
            continue
    else:
        print("You either enter two words or incorrect letter")
        lives -= 1
        print("Life remaining:", lives)
        if lives == 0:
            print("You have lost game")
            break
        else:
            continue
